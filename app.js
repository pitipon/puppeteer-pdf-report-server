const puppeteer = require('puppeteer');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

if (process.env.NODE_ENV !== 'production') {
    const morgan = require('morgan');
    app.use(morgan('combined'));
}

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));
app.use(bodyParser.json());

app.post('/', async (req, res, next) =>{
    try {
        let pdfBinary = await generatePdf(req.body.html, req.body.pdf);

        res.type('application/pdf');
        res.end(pdfBinary, 'binary');
    } catch (e) {
        next(e);
    }
});

async function generatePdf(htmlBody, pdfConfig) {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    await page.goto(`data:text/html,${htmlBody}`, {waitUntil: 'networkidle0'});
    let pdfBinary = await page.pdf(pdfConfig);
   
    await browser.close();
    
    return pdfBinary;
}

// ===== Error Handler
app.use(function (err, req, res, next) {
    console.error(err);
    res.send(err);
})


// ===== Node Server
const port = process.env.PORT || 7000;
app.listen(port, ()=>console.log(`Server started at port ${port}`));
